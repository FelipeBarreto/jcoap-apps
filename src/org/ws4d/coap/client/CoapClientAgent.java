package org.ws4d.coap.client;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.ws4d.coap.Constants;
import org.ws4d.coap.connection.BasicCoapChannelManager;
import org.ws4d.coap.interfaces.CoapChannelManager;
import org.ws4d.coap.interfaces.CoapClient;
import org.ws4d.coap.interfaces.CoapClientChannel;
import org.ws4d.coap.interfaces.CoapResponse;

public class CoapClientAgent implements CoapClient {
    private static final String SERVER_ADDRESS = "localhost";
    private static final int PORT = Constants.COAP_DEFAULT_PORT;

    CoapChannelManager channelManager = null;
    CoapClientChannel clientChannel = null;

    public static void main(String[] args) {
        System.out.println("Start CoAP Client");
        CoapClientAgent client = new CoapClientAgent();
        client.channelManager = BasicCoapChannelManager.getInstance();
        client.runCoapClient();
    }
    
    public void runCoapClient(){
    	try {
			clientChannel = channelManager.connect(this, InetAddress.getByName(SERVER_ADDRESS), PORT);
			
			// Your stuff here
			// E.g., Create a CoAP Request and send it to the server
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
    }

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		System.out.println("Connection Failed");
	}

	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		System.out.println("Received Response: " + response.toString());
		if(response.getPayload().length > 0){
			System.out.println("Response Payload: " + new String(response.getPayload()));
		}
		else{
			System.out.println("Empty Payload");
		}
	}
}
