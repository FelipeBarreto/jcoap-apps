package org.ws4d.coap.server;

import org.ws4d.coap.rest.CoapResourceServer;

public class CoapServer {

	private CoapResourceServer resourceServer;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Start Sample Resource Server");
		CoapServer sampleServer = new CoapServer();
		sampleServer.run();
	}

	private void run() {
		resourceServer = new CoapResourceServer();
		
		// Add the server resources here
		// Dynamic resources can be created by clients using the POST method
		
		try {
			resourceServer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// You can put here something to test the server resources
		// E.g., modify the values and notify all the observers
	}
}
